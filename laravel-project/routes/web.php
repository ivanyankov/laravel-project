<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::group(['middleware' => 'guest'], function(){
	Route::view('login','login')->name('login');
	Route::post('login','Auth\LoginController@Login');

	Route::view('register','register')->name('register-user');
	Route::post('register','Auth\RegisterController@StoreUser')->name('register-store');
});


Route::group(['middleware'=>'auth'], function(){
	// Posts routes
	Route::get('/posts','PostController@Index')->name('posts.index');
	// Blog routes
	Route::get('/blog', 'BlogController@index')->name('blog.posts-listing');
	Route::get('/blog/show/{id}', 'BlogController@show')->name('blog.show'); 
	// Auth routes
	Route::get('/logout','Auth\LoginController@Logout')->name('logout');
	Route::group(['middleware' => 'adminCheck'], function(){
		// Post routes
        Route::get('/create', 'PostController@create')->name('post.create');
        Route::post('/create', 'PostController@store')->name('post.create');
        Route::get('/post/show/{id}', 'PostController@show')->name('post.show');
        Route::get('/post/edit/{id}', 'PostController@edit')->name('post.edit');
        Route::put('/post/edit/{id}', 'PostController@update')->name('post.update');
        Route::delete('/create/delete/{id}', 'PostController@destroy')->name('post.destroy');
	});
});