<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'post_excerpt', 'post_content',
    ];

    /**
     * Database table name
     */
    protected $table = 'posts';

    /**
     * Get the post that owened by admin.
     */
    public function user()
    {
        return $this->belongsTo('App\Entities\User');
    }
}