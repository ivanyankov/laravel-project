<?php

namespace App\Http\Controllers;

use App\Entities\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        return view('posts.create', compact('posts'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        $validator = Validator::make($request->all(), [
            'title'     => 'required|min:10',
            'excerpt'   => 'required|min:10|max:150',
            'content'   => 'required|min:10'
        ]);


        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->messages())
                        ->withInput();
        } 

        $post = new Post;
        $post->title        = $request->title;
        $post->user_id = Auth::user()->id;
        $post->post_excerpt = $request->excerpt;
        $post->post_content = $request->content;

        //save the post
        $post->save();

        Session::flash('message', 'Successfully created a post!');

        return redirect('create');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the post
        $post = Post::find($id);

        // show the view and pass the post to it
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // get the post
         $post = Post::find($id);

         // show the view and pass the post to it
         return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'     => 'required|min:10',
            'excerpt'   => 'required|min:10|max:150',
            'content'   => 'required|min:10'
        ]);


        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->messages())
                        ->withInput();
        } 

        $post = new Post;

        $post->title        = $request->title;
        $post->post_excerpt = $request->excerpt;
        $post->post_content = $request->content;

        //save the post
        $post->save();

        return redirect('create');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $post = Post::find($id);
        $post->delete();

        // session
        Session::flash('message', 'Successfully deleted the nerd!');
        
        return redirect('create');
    }
}
