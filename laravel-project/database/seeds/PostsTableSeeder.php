<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use App\Entities\User;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,20) as $index) {
            DB::table('users')->insert([
                'name'         => $faker->name($gender = 'male'|'female'),
                'email'       => $faker->safeEmail,
                'isAdmin'      => $faker->numberBetween($min = 0, $max = 1),
                'password'      => $faker->password,
                'created_at' =>$faker->dateTime($max = 'now'),
                'updated_at' =>$faker->dateTime($max = 'now')
            ]);
            
	        DB::table('posts')->insert([
                'title'         => $faker->realText($maxNbChars = 50, $indexSize = 2),
                'user_id'       => $faker->randomElement(User::pluck('id')->toArray()),
                'post_excerpt'  => $faker->realText($maxNbChars = 100, $indexSize = 2),
                'post_content' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                'created_at' =>$faker->dateTime($max = 'now'),
                'updated_at' =>$faker->dateTime($max = 'now')
            ]);
            
        }
    }
}
