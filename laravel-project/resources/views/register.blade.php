@extends('layouts.app')

@section('content')
<div class="container hero-section" style="background-image: url('{{ asset("images/paperplane.jpg") }}');">
	<div class="row">
		<div class="col-md-6 offset-3">
			<form method="POST" class="register-form">
				<div class="form-group">
					<label for="emailInput">Enter email address:</label>
					<input id="emailInput" class="form-control" type="text" name="email" placeholder="Email address" value="{{old('email')}}"/>
				</div>
				<div class="form-group">
					<label for="inputName">Enter name:</label>
					<input id="inputName" class="form-control" type="text" name="name" placeholder="Name" value="{{old('name')}}"/>
				</div>
				<div class="form-group">
					<label for="inputPassword">Enter password:</label>
					<input id="inputPassword" class="form-control" type="password" name="password" placeholder="Enter password" />
				</div>
				<div class="form-group">
					<label for="inputRepeatPassword">Repeat password:</label>
					<input id="inoutRepeatPassword" class="form-control" type="password" name="passwordConfirmed" placeholder="Enter password again" />	
				</div>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-lg btn-primary">Register</button>
				</div>
				{{csrf_field()}}
			</form>
		</div>
	</div>
</div>

@endsection