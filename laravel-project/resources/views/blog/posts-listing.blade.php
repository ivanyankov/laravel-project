<!-- Extends the layout -->
@extends('layouts.app')

@section('title', 'Blog page')

@section('content')
    <div class="container blog-listing">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Checkout our latest blog posts</h1>
            </div>
        </div>
        <div class="row">
            @foreach($posts as $post)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">
                <div class="post-box">
                    <h3 class="title">{{ $post->title }}</h3>
                    <p class="excerpt">{{ $post->post_excerpt }}</p>
                    <p class="author">{{ $post->user->name }}<span class="date-created">{{ $post->created_at }}</span></p>
                    <a class="btn btn-md btn-primary" href="{{ route('blog.show', $post->id) }}">Read more</a>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="post-pagination">
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection