<!-- Extends the layout -->
@extends('layouts.app')

@section('title', 'Welcome page')

@section('content')
    <div class="container text-center " >
        <div class="jumbotron hero-section" style="background-image: url('{{ asset("images/developers.jpg") }}');">
            <h1 class="welcome-title">Welcome to our Laravel blog</h1>
            <p class="lead">Checkout our Laravel uni project. Simple and elengant blog app.</p>
            <hr class="my-4">
            <p>This is a university project. We are robots and we will take the world!</p>
            <p class="lead">
                <a class="btn btn-dark btn-lg" href="{{ route('blog.posts-listing') }}" role="button">Blog</a>
            </p>
        </div>
    </div>
@endsection