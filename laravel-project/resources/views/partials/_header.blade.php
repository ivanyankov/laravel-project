<div class="container">
    <div class="row">
        <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <a class="navbar-brand" href="/">Laravel Blog</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                        </button>
                      
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                                    <a class="nav-link" href="/">Home</a>
                                </li>
                            @auth
                                <li class="nav-item {{ Request::is('blog') ? 'active' : '' }}">
                                    <a class="nav-link" href="{{ route('blog.posts-listing') }}">Blog</a>
                                </li>
                            @endauth
                            </ul>
                            <ul class="navbar-nav ml-auto">
                                @guest
                                    <li class="nav-item {{ Request::is('login') ? 'active' : '' }}">
                                        <a class="nav-link" href="{{ route('login') }}">Login</a>
                                    </li>
                                    <li class="nav-item {{ Request::is('register') ? 'active' : '' }}">
                                        <a class="nav-link" href="{{ route('register-user') }}">Register</a>
                                    </li>
                                @endguest
                                @auth
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ Auth::user()->name }}
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            @if( Auth::user()->isAdmin === 1 )
                                                <a class="dropdown-item" href="{{ route('post.create') }}">Create post</a>
                                                <div class="dropdown-divider"></div>
                                            @endif
                                            <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                                        </div>
                                    </li>
                                @endauth
                            </ul>
                        </div>
                </nav>
        </div>
    </div>
</div>
