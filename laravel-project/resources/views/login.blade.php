@extends('layouts.app')

@section('content')

<div class="container hero-section" style="background-image: url('{{ asset("images/paperplane.jpg") }}');">
	<div class="row">
		<div class="col-md-6 offset-3">
			<form method="POST" class="login-form">
				<div class="form-group">
					<label for="emailInput">Email address:</label>
					<input id="emailInput" class="form-control" type="text" name="email" placeholder="Email..." value="{{old('email')}}"/>
				</div>
				<div class="form-group">
					<label for="passwordInput">Password:</label>
					<input id="passwordInput" class="form-control" type="password" name="password" placeholder="Enter password..." />
				</div>
				<div class="form-group text-center">
					<button type="submit" class="btn btn-lg btn-primary">Sign in</button>
				</div>
				{{csrf_field()}}
			</form>
		</div>
	</div>
</div>

@endsection