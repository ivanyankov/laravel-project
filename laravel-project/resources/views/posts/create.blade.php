@extends('layouts.app') 

@section('title', 'Create post') 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="welcome-title">Create a post</h1>
        </div>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @elseif ( session('message') )
    <div class="alert alert-success">
        {{{ session('message') }}}
    </div>
    @endif
    <form action="/create" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="postTitle">Title</label>
            <input name="title" type="text" class="form-control" id="postTitle" placeholder="Post title">
        </div>
        <div class="form-group">
            <label for="postExcerpt">Excerpt</label>
            <input name="excerpt" type="text" class="form-control" id="postExcerpt" placeholder="Post excerpt">
        </div>
        <div class="form-group">
            <label for="postContent">Content</label>
            <textarea name="content" class="form-control" id="postContent" rows="3" placeholder="Post content"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Create post</button>
    </form>
    <br>
    <br>
</div>
<div class="container">
    <table class="table table-striped table-dark">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Excerpt</th>
                <th scope="col">Created at</th>
                <th scope="col">Updated at</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $counter = 0;    
            ?> 
            @foreach($posts as $post)
            <?php $counter++; ?>
            <tr>
                <th scope="row">{{ $counter }}</th>
                <td>{{ $post->title }}</td>
                <td>{{ $post->post_excerpt }}</td>
                <td>{{ $post->created_at }}</td>
                <td>{{ $post->updated_at }}</td>
                <td>
                <a href="{{ route('post.show', $post->id) }}" class="btn btn-primary">View</a>
                <a href="{{ route('post.edit', $post->id) }}" class="btn btn-info">Edit</a>
                <form action="{{ route('post.destroy', $post->id) }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-danger">Delete</button>
                </form>
                
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-12">
            {{ $posts->links() }}
        </div>
    </div>
</div>
@endsection