@extends('layouts.app') 

@section('title', 'Create post') 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="welcome-title">Update the post</h1>
        </div>
    </div>
    
    <form action="{{ route('post.update', $post->id) }}" method="POST">
        {{ csrf_field() }}
        <input name="_method" type="hidden" value="PUT">
        <div class="form-group">
            <label for="postTitle">Title</label>
            <input name="title" type="text" class="form-control" id="postTitle" placeholder="Post title" value="{{ $post->title }}">
        </div>
        <div class="form-group">
            <label for="postExcerpt">Excerpt</label>
            <input name="excerpt" type="text" class="form-control" id="postExcerpt" placeholder="Post excerpt" value="{{ $post->post_excerpt }}">
        </div>
        <div class="form-group">
            <label for="postContent">Content</label>
            <textarea name="content" class="form-control" id="postContent" rows="3" placeholder="Post content">{{ $post->post_content }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
    <br>
    <br>
</div>
@endsection