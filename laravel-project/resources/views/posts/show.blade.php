@extends('layouts.app') 

@section('title', 'View post') 

@section('content')
<div class="container">
    <div class="jumbotron">
        <h1 class="display-5">{{ $post->title }}</h1>
        <p class="lead">{{ $post->post_excerpt }}</p>
        <hr class="my-4">
        <p>{!! $post->post_content !!}</p>
        <div class="row">
            <div class="col-md-2">
                <p>Created: {{ $post->created_at }}</p>
            </div>
            <div class="col-md-2">
                <p>Updated: {{ $post->updated_at }}</p>
            </div>
        </div>
    </div>
</div>
@endsection